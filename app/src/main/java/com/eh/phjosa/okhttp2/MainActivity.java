package com.eh.phjosa.okhttp2;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ScrollView;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okio.Buffer;
import okio.Sink;
import okio.Timeout;

public class MainActivity extends AppCompatActivity {
    public static final String CLOCK_STREAM = "https://http2.golang.org/clockstream";
    private static final String TAG = MainActivity.class.getSimpleName();
    private OkHttpClient client;
    private ArrayAdapter arrayAdapter;
    private ListView listView;
    private ArrayList<String> arrayList;
    private int counter = 0;
    private ScrollView scrollView;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        scrollView = findViewById(R.id.scrollView);
        arrayList = new ArrayList<>();
        arrayList.add("initializing...");

        listView = findViewById(R.id.listview);

        arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayList);

        if (listView != null) {
            listView.setAdapter(arrayAdapter);
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                getRequest();
            }
        }).start();

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @NonNull
    private Request getRequest() {


        client = new OkHttpClient();


        Request request = new Request.Builder()
                .url(CLOCK_STREAM)
                .build();

        Call call = client.newCall(request);


        try (Response response = call.execute()) {
            int bufferSize = 100;
            int offset = 0;
            final String newline = "\n";
            final byte[] buffer = new byte[bufferSize];
            final StringBuffer output = new StringBuffer();
            while (true) {
                offset = response.body().source().read(buffer, 0, bufferSize);

                final String c = new String(buffer);
                Log.d(TAG, "C: " + c.split(newline)[0]);


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        arrayList.add(counter++ + ": " + c.split(newline)[0]);
                        arrayAdapter.notifyDataSetChanged();
                        listView.setSelection(arrayAdapter.getCount() - 1);
                        output.delete(0, output.length());
                    }
                });


//                if (!c.equals(newline)) {
//                    output.append(c);
//                } else {
//
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            arrayList.add(counter++ + ": " + output.toString());
//                            arrayAdapter.notifyDataSetChanged();
//                            listView.setSelection(arrayAdapter.getCount() - 1);
//                            output.delete(0, output.length());
//                        }
//                    });
//
//                }

            }

//            response.body().source().readAll(new Sink() {
//                @Override
//                public void write(final Buffer source, long byteCount) throws IOException {
//                    Buffer buffer = new Buffer();
////                    while (!source.exhausted()) {
////
////                        source.require(byteCount);
////                        source.read(buffer, byteCount);
////                        Log.d(TAG, "read buffer: " + source);
////                    }
//
////                    source.require(byteCount);
////                    source.read(buffer, byteCount);
//                    Log.d(TAG, "read buffer: " + source);
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            arrayList.add(counter++ + ": " + source.toString());
//                            arrayAdapter.notifyDataSetChanged();
//                            listView.setSelection(arrayAdapter.getCount() - 1);
//                        }
//                    });
//
//
//                }
//
//                @Override
//                public void flush() throws IOException {
//                    Log.d(TAG, "flush ");
//                }
//
//                @Override
//                public Timeout timeout() {
//                    Log.d(TAG, "timeout ");
//                    return null;
//                }
//
//                @Override
//                public void close() throws IOException {
//                    Log.d(TAG, "close ");
//                }
//            });
        } catch (Exception e) {
            Log.d(TAG, "error: message: " + e.getMessage());
            Log.d(TAG, "error: message: " + e.getCause());
        }

        return request;
    }
}
